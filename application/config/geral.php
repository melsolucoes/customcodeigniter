<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Depuração no rodapé
|--------------------------------------------------------------------------
*/
$config['enable_profiling'] = TRUE; #TRUE permite depuração no rodapé/ FALSE retira depuração


/*
|--------------------------------------------------------------------------
| Caminhos
|--------------------------------------------------------------------------
*/
$config['imagens'] = 'asset/imagens/';
$config['js'] = 'asset/js/';
$config['css'] = 'asset/css/';
$config['bootstrap'] = 'asset/bootstrap/';