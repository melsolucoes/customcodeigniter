<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/* Debug no Rodape */
$hook['post_controller_constructor'][] = array(
    'class'    => 'ProfilerEnabler',
    'function' => 'EnableProfiler',
    'filename' => 'hooks.classes.php',
    'filepath' => 'hooks',
    'params'   => array()
);



/* config/hooks.php 
LOG ALL QUERIES: http://sajanmaharjan.com.np/2013/02/05/code-igniter-log-all-queries/
*/
$hook['post_system'][] = array(
    'class' => 'LogQueryHook',
    'function' => 'log_queries',
    'filename' => 'LogQueryHook.php',
    'filepath' => 'hooks'
);