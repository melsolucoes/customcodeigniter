<?php

/* welcome.php.twig */
class __TwigTemplate_fb17291db34064e05cc0e9f1945d78ba84bd28a1e8a333ffc3a246e1af862f60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"<?php echo base_url() . \$this->config->item('bootstrap'); ?>css/bootstrap.min.css\" />

<script type=\"text/javascript\" src=\"<?php echo base_url() . \$this->config->item('bootstrap'); ?>js/bootstrap.min.js\"></script>

<h1>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["texto"]) ? $context["texto"] : null), "html", null, true);
        echo "</h1>";
    }

    public function getTemplateName()
    {
        return "welcome.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }
}
/* <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() . $this->config->item('bootstrap'); ?>css/bootstrap.min.css" />*/
/* */
/* <script type="text/javascript" src="<?php echo base_url() . $this->config->item('bootstrap'); ?>js/bootstrap.min.js"></script>*/
/* */
/* <h1>{{texto}}</h1>*/
