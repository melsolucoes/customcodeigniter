###################
Está é uma Customização do Codeigniter
###################

O proposito aqui é criar uma extensão pessoal do Framework CodeIgniter com todas as funcionalidades básicas para a criação de um projeto web.

Essa extensão inclui funcionalidades como:

* Composer

* Twig Template

* Banco de Dados PostgreSql

* Log de Erros e Query

* URL Amigável

* HMVC